/*
pub fn init() {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init_with_level(log::Level::Debug).expect("error initializing console_log");

    log::info!("WASM initialized.");
}
*/

#[no_mangle]
pub extern "C" fn add(x: i32, y: i32) -> i32 { x + y }