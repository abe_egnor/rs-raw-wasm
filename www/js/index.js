function main() {
    WebAssembly.instantiateStreaming(fetch("target/wasm32-unknown-unknown/debug/rs_raw_wasm.wasm"))
    .then(wasm => console.log(wasm.instance.exports.add(1, 2)));
}

main()